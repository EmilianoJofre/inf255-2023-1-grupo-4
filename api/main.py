from fastapi import FastAPI
from starlette.responses import FileResponse
from api.database import Database
from fastapi.staticfiles import StaticFiles

app = FastAPI()

# Configurar la instancia de la base de datos
db = Database()

# Ruta raíz
@app.get("/")
def read_root():
    return {"Hello": "World"}

# Ruta para servir el archivo index.html
@app.get("/home")
async def read_home():
    return FileResponse("static/index.html")

# Montar el directorio estático para servir archivos estáticos
app.mount("/static", StaticFiles(directory="static"), name="static")