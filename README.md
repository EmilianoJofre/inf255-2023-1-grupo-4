# INF255-2023-1-GRUPO-4

Este es el repositorio del Grupo 4, cuyos integrantes son:

# GRUPO 4
* Aaron Vargas
* Alan Yanine
* Emiliano Jofré - 201804167-k
* **Tutor**: Ernesto  

# Wiki
Puede acceder a la wiki mediante el siguiente [enlace](https://gitlab.com/EmilianoJofre/inf255-2023-1-grupo-4/-/wikis/home)

# Videos
* [Video presentacion Hito 1](https://www.youtube.com/watch?v=BIi-CD36GTw&feature=youtu.be&ab_channel=VideoUploader)

# Aspectos técnicos relevantes

_Todo aspecto relevante cuando para poder usar el proyecto o consideraciones del proyecto base a ser entregado_

## Estructura del proyecto

El proyecto está organizado de la siguiente manera:

- `api/`: Carpeta que contiene el código fuente de la API.
  - `main.py`: Archivo principal que define las rutas y funcionalidades de la API.
  - `database.py`: Archivo que contiene la configuración y funciones de acceso a la base de datos.
  - ...
- `static/`: Carpeta que contiene archivos estáticos como imágenes, hojas de estilo, etc.
  - `index.html`: Archivo HTML principal de la interfaz de usuario.
  - ...
- `README.md`: Archivo que contiene información sobre el proyecto, su estructura y cómo usarlo.
- `requirements.txt`: Archivo que especifica las dependencias del proyecto.
- `PresentaciónHito1.pdf`: Archivo de presentación del hito 1.
- `PresentaciónHito3.pdf`: Archivo de presentación del hito 3.

## Instalación

Para ejecutar el proyecto, se recomienda seguir los siguientes pasos:

1. Clonar el repositorio en tu máquina local:
git clone https://gitlab.com/EmilianoJofre/inf255-2023-1-grupo-4.git

2. Crear un entorno virtual e instalar las dependencias:
```bash
cd python-mongodb-crud
conda create --name python-mongo python=3
# Para Windows
pip install -r requirements.txt
```

3. Ejecutar el proyecto:
`uvicorn app:app --reload`

4. Acceder a la API desde tu navegador o herramienta de desarrollo en la dirección `http://127.0.0.1:8000/`.
¡Listo! Ahora puedes explorar y utilizar la API del proyecto.

